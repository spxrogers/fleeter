import React from 'react'
import { ButtonLink, HtmlHead } from 'src/components'

export const ComingSoon = () => {
  return (
    <div className="flex flex-col items-center justify-center min-h-screen py-2">
      <HtmlHead />

      <main className="flex flex-col items-center justify-center w-full flex-1 px-20 text-center">
        <p className="mt-3 text-2xl">
          <code className="p-3 font-mono text-lg bg-gray-100 rounded-md">Coming soon...</code>
        </p>
        <br />
        <ButtonLink href={'/'} text={'Return home'} isSecondary />
      </main>
    </div>
  )
}
