import React from 'react'

export type TextInputProps = {
  value: string
  onChange: (newValue: string) => void
  placeholder?: string
  disabled?: boolean
}

export const TextInput = ({ value, onChange, placeholder = 'Enter text...', disabled = false }: TextInputProps) => (
  <input
    className={'rounded-lg px-4 py-2 border-2 border-primary-500 disabled:opacity-50 disabled:hover:cursor-not-allowed'}
    type={'text'}
    placeholder={placeholder}
    value={value}
    onChange={(event) => onChange(event.target.value ?? '')}
    disabled={disabled}
  />
)
