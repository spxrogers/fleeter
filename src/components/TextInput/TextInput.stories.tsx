import React, { useState } from 'react'
import { ComponentMeta, ComponentStory } from '@storybook/react'
import { TextInput } from './'

type ExampleProps = {
  placeholder?: string
  disabled?: boolean
}

const TextInputApp = ({ placeholder, disabled }: ExampleProps) => {
  const [text, setText] = useState('')
  return (
    <>
      <TextInput value={text} onChange={setText} placeholder={placeholder} disabled={disabled} />
    </>
  )
}

export default {
  title: 'Components/TextInput',
  component: TextInputApp,
} as ComponentMeta<typeof TextInputApp>

const Template: ComponentStory<typeof TextInputApp> = (args) => <TextInputApp {...args} />

export const Example = Template.bind({})
Example.args = {
  placeholder: 'Placeholder text',
  disabled: false,
}
