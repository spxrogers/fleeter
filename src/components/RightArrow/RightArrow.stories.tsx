import React from 'react'
import { ComponentMeta } from '@storybook/react'
import { RightArrow } from './'

export default {
  title: 'Components/RightArrow',
  component: RightArrow,
} as ComponentMeta<typeof RightArrow>

export const Example = () => <RightArrow />
