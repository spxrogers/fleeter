import React from 'react'
import { PropsWithChildren } from 'react'
import NextLink from 'next/link'

export type LinkProps = {
  href: string
  newTab?: boolean
}

export const Link = ({ children, href, newTab = false }: PropsWithChildren<LinkProps>) =>
  newTab ? (
    <a href={href} target={'_blank'} rel="noopener noreferrer">
      {children}
    </a>
  ) : (
    <NextLink href={href}>
      <a>{children}</a>
    </NextLink>
  )
