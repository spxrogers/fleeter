import React from 'react'
import { ComponentMeta, ComponentStory } from '@storybook/react'
import { Link } from './'

export default {
  title: 'Components/Link',
  component: Link,
} as ComponentMeta<typeof Link>

const Template: ComponentStory<typeof Link> = (args) => <Link {...args} />

export const CurrentTab = Template.bind({})
CurrentTab.args = {
  href: 'https://fleeter.dev',
  children: <span className="border border-2 border-primary-500 p-3">Go to Fleeter</span>,
}

export const NewTab = Template.bind({})
NewTab.args = {
  href: 'https://fleeter.dev',
  newTab: true,
  children: <span className="border border-2 border-primary-500 p-3">Open Fleeter in a new tab</span>,
}
