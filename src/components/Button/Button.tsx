import React from 'react'

export enum ButtonVariant {
  Primary = 'primary',
  Secondary = 'secondary',
  Danger = 'danger',
}

export type ButtonProps = {
  text: string
  onClick: () => void
  variant?: ButtonVariant
}

export const Button = ({ text, onClick, variant = ButtonVariant.Primary }: ButtonProps) => (
  <button className={ButtonVariantStyles[variant]} onClick={onClick}>
    {text}
  </button>
)

const ButtonVariantStyles: { [key in ButtonVariant]: string } = {
  [ButtonVariant.Primary]: 'rounded-lg px-4 py-2 bg-primary-500 text-white hover:bg-primary-400',
  [ButtonVariant.Secondary]:
    'rounded-lg px-4 py-2 border-2 border-secondary-500 text-secondary-500 hover:bg-secondary-500 hover:text-white',
  [ButtonVariant.Danger]: 'rounded-lg px-4 py-2 bg-alert-500 text-white hover:bg-alert-400',
}
