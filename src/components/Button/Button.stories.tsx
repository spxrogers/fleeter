import React, { useState } from 'react'
import { ComponentMeta, ComponentStory } from '@storybook/react'
import { Button, ButtonProps, ButtonVariant } from './'

const ButtonApp = (props: ButtonProps) => {
  const [clickCount, setClickCount] = useState(0)
  return (
    <>
      <Button {...props} text={'Button'} onClick={() => setClickCount((prevState) => prevState + 1)} />
      <br />
      Click count: <strong>{clickCount}</strong>
    </>
  )
}

export default {
  title: 'Components/Button',
  component: ButtonApp,
  argTypes: {
    variant: {
      control: {
        type: 'select',
        options: ButtonVariant,
      },
    },
  },
} as ComponentMeta<typeof ButtonApp>

const Template: ComponentStory<typeof ButtonApp> = (args) => <ButtonApp {...args} />

export const Primary = Template.bind({})
Primary.args = {
  variant: ButtonVariant.Primary,
}

export const Secondary = Template.bind({})
Secondary.args = {
  variant: ButtonVariant.Secondary,
}

export const Danger = Template.bind({})
Danger.args = {
  variant: ButtonVariant.Danger,
}
