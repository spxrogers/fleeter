import React from 'react'
import Head from 'next/head'

export type HtmlHeadProps = {
  title?: string
  description?: string
}

export const HtmlHead = ({ title = 'Fleeter', description = 'Turn your Tweets into Fleets' }: HtmlHeadProps) => (
  <Head>
    <title>{title}</title>
    <meta name="description" content={description} />
    <link rel="icon" href="/favicon.ico" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto+Condensed&display=optional" />
  </Head>
)
