import React from 'react'
import { ComponentMeta, ComponentStory } from '@storybook/react'
import { ButtonLink } from './'

export default {
  title: 'Components/ButtonLink',
  component: ButtonLink,
} as ComponentMeta<typeof ButtonLink>

const Template: ComponentStory<typeof ButtonLink> = (args) => <ButtonLink {...args} />

export const Primary = Template.bind({})
Primary.args = {
  text: 'I am a link',
  href: '#',
}

export const Secondary = Template.bind({})
Secondary.args = {
  text: 'I am a link',
  href: '#',
}

export const Fleeter = Template.bind({})
Fleeter.args = {
  text: 'Go to Fleeter',
  href: 'https://fleeter.dev',
}

export const FleeterNewTab = Template.bind({})
FleeterNewTab.args = {
  text: 'Open Fleeter in a new tab',
  href: 'https://fleeter.dev',
  newTab: true,
  isSecondary: true,
}
