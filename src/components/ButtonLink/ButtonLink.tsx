import React from 'react'
import { Link, LinkProps } from 'src/components'

export type ButtonLinkProps = LinkProps & {
  text: string
  isSecondary?: boolean
}

export const ButtonLink = ({ text, href, newTab = false, isSecondary = false }: ButtonLinkProps) => (
  <Link href={href} newTab={newTab}>
    {isSecondary ? (
      <button
        className={'rounded-lg px-4 py-2 border-2 border-blue-500 text-blue-500 hover:bg-blue-500 hover:text-white'}
      >
        {text}
      </button>
    ) : (
      <button className={'rounded-lg px-4 py-2 bg-blue-500 text-white hover:bg-blue-400'}>{text}</button>
    )}
  </Link>
)
