/*
  Warnings:

  - A unique constraint covering the columns `[twitterIdStr]` on the table `User` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[handle]` on the table `User` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX `User.twitterIdStr_unique` ON `User`(`twitterIdStr`);

-- CreateIndex
CREATE UNIQUE INDEX `User.handle_unique` ON `User`(`handle`);
