-- AlterTable
ALTER TABLE `Donation` ADD COLUMN `comment` VARCHAR(191),
    ADD COLUMN `guest` BOOLEAN NOT NULL DEFAULT false,
    MODIFY `userId` BIGINT;
