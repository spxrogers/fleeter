import { PrismaClient } from '@prisma/client'
import { CreateUserInput, userTable } from 'db/user'

let prisma: PrismaClient | null = null

const isReady = () => prisma !== null

const ensureClientInit = () => {
  if (!isReady()) {
    initClient()
  }
}

const initClient = () => {
  prisma = new PrismaClient()
}

const getClient = () => {
  if (!isReady()) {
    throw new Error('Must call initDbClient before calling getDbClient.')
  }
  return prisma as PrismaClient
}

const call = async <T>(query: () => Promise<T>, defaultValue?: T) => {
  ensureClientInit()
  try {
    return await query()
  } catch (e) {
    console.error('Error executing DB query', e)
    if (defaultValue !== undefined) {
      return defaultValue
    }
    throw e
  }
}

export const db = {
  meta: {
    ensureClientInit,
    initClient,
    isReady,
    getClient,
  },
  user: {
    getUserCount: async () => await call(() => userTable.getUserCount(getClient()), -1),
    getUserWithDonations: async (userId: bigint) =>
      await call(() => userTable.getUserWithDonations(getClient(), userId)),
    createUser: async (createUserInput: CreateUserInput) =>
      await call(() => userTable.createUser(getClient(), createUserInput)),
  },
}
