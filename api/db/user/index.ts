import { PrismaClient, User, Donation } from '@prisma/client'

const getUserCount = async (client: PrismaClient): Promise<number> => await client.user.count()

const getUserWithDonations = async (
  client: PrismaClient,
  userId: bigint
): Promise<(User & { donation: Donation[] }) | null> =>
  await client.user.findUnique({
    where: {
      id: userId,
    },
    include: {
      donation: true,
    },
  })

export interface CreateUserInput {
  twitterIdStr: string
  handle: string
}

const createUser = async (client: PrismaClient, { twitterIdStr, handle }: CreateUserInput) => {
  const createResult = await client.user.create({
    data: {
      twitterIdStr,
      handle,
      handleAtSignup: handle,
    },
  })
  console.log('Created new user', createResult)
}

export const userTable = {
  getUserCount: getUserCount,
  getUserWithDonations: getUserWithDonations,
  createUser: createUser,
}
