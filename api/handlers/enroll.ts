import { Handler } from '@netlify/functions'
import { db } from 'db/index'
import { getRandomNumber } from 'src/randomNumber'

export const handler: Handler = async (event, context) => {
  db.meta.initClient()

  const twitterIdStr = event.queryStringParameters?.twitterIdStr
  const handle = event.queryStringParameters?.handle
  const createParamsValid = [twitterIdStr, handle].every((val) => !!val && val.length > 0)
  if (createParamsValid) {
    const createInput = {
      twitterIdStr: twitterIdStr as string,
      handle: handle as string,
    }
    console.log('Creating a new user', createInput)
    await db.user.createUser(createInput)
    console.log('Created a new user', createInput)
  } else {
    console.log('No user being created', {
      input: {
        twitterIdStr: twitterIdStr ?? 'null',
        handle: handle ?? 'null',
      },
    })
  }

  return {
    statusCode: 200,
    body: jsonWithBigIntStrings({
      message: '🤙',
      randomNumber: getRandomNumber(),
      dbReady: db.meta.isReady(),
      userCount: await db.user.getUserCount(),
      user: await db.user.getUserWithDonations(2n),
    }),
  }
}

const jsonWithBigIntStrings = (data: unknown) =>
  JSON.stringify(data, (key, value) => (typeof value === 'bigint' ? value.toString() : value))
