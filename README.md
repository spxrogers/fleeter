# fleeter.dev

> **https://fleeter.dev**
> 
> **https://storybook.fleeter.dev/**
>
> https://gitlab.com/spxrogers/fleeter/
> 
> https://app.netlify.com/sites/fleeter/
>
> https://app.netlify.com/sites/fleeter-storybook/

|Site|Build Status|
|----------|-----------|
|`https://fleeter.dev`|[![Site Build Status](https://api.netlify.com/api/v1/badges/4ad2ea6e-da1f-492d-9f06-2d36b8dd4587/deploy-status)](https://app.netlify.com/sites/fleeter/deploys)|
|`https://storybook.fleeter.dev`|[![Storybook BUild Status](https://api.netlify.com/api/v1/badges/d23d76e6-ab62-4260-a7f6-7ca651108ce5/deploy-status)](https://app.netlify.com/sites/fleeter-storybook/deploys)|

## What?

A service to turn your Tweets into _Fleets_. 

1. Login to Fleeter with your Twitter account.
2. Authorize Fleeter to your Twitter account.
3. Configure your maximum Tweet age in days.
4. (Optional) Configure what time of day you want Fleeter to find too-old tweets and delete them.
5. Fleeter runs daily, in the background, keeping your Twitter account fresh of old fleeting tweets.

## Technical

- [Next.js](https://nextjs.org/) web app.
    - [Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Netlify](https://nextjs.org/) deployment.
- [Storybook](https://storybook.js.org) component development playground.

### Developing

1. Install dependencies with `npm i`.
2. Make changes locally and test+verify with NPM scripts, listed below.

#### NPM Scripts

|Script|Description|
|----------|-----------|
|`build`|Cleans the output directory (`.next/`) and builds the production bundle. **What Netlify executes (Site).**|
|`storybook:build`|Build the static storybook website and outputs to `storybook-static/`. **What Netlify executes (Storybook).**|
|`bundle`|Builds the production bundle.|
|`clean`|Cleans the output directory (`.next/`).|
|`lint`|Lints/formats source files.|
|`serve:dev`|Starts a local dev server, hot-reloading changes on save.|
|`serve:prod`|Builds the production bundle and serves it on localhost.|
|`storybook:serve`|Builds the storybook site locally and serves it on localhost, hot-reloading changes on save.|
