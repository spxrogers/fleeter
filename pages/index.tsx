import React from 'react'
import { NextPage } from 'next'
import { HtmlHead, Image, Link, NBSP, RightArrow } from 'src/components'

const Home: NextPage = () => {
  return (
    <div className="flex flex-col items-center justify-center min-h-screen py-2">
      <HtmlHead />

      <main className="flex flex-col items-center justify-center w-full flex-1 px-20 text-center">
        <h1 className="text-6xl font-bold">
          <a href="https://fleeter.dev" className="text-primary-600 hover:text-primary-700">
            Fleeter
          </a>
        </h1>

        <p className="mt-3 text-2xl">
          <code className="p-3 font-mono text-lg bg-gray-100 rounded-md">Fleeting : temporary tweets</code>
        </p>

        <div className="flex flex-wrap items-center justify-around max-w-4xl mt-6 sm:w-full">
          <Link href="/enroll">
            <HomeTile title="Signup" text="Sign up for Fleeter." />
          </Link>

          <Link href="/learn">
            <HomeTile title="Learn" text="More information about Fleeter." />
          </Link>

          <Link href="/manage">
            <HomeTile title="Manage Account" text="Enrolled in Fleeter and want to update your settings or disable?" />
          </Link>

          <Link href="/faq">
            <HomeTile title="FAQ" text="You have questions. We may have answers." />
          </Link>
        </div>
      </main>

      <footer className="flex items-center justify-center w-full h-10 border-t">
        <Link
          href={
            'https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app'
          }
          newTab
        >
          Bootstrapped by Nextjs
          <NBSP />
          <span className="h-4 ml-2">
            <Image src="/vercel.svg" alt="Vercel Logo" width={72} height={16} />
          </span>
        </Link>
      </footer>
      <footer className="flex justify-end w-full h-5">
        <Link href={'https://storybook.fleeter.dev'} newTab={true}>
          <span className="text-accent-700 font-thin text-xs">(view component storybook)</span>
        </Link>
      </footer>
    </div>
  )
}

type HomeTileProps = {
  title: string
  text: string
}

const HomeTile = ({ title, text }: HomeTileProps) => (
  <div className="p-6 mt-6 text-left border w-96 rounded-xl hover:border-secondary-600 hover:text-primary-600 focus:text-primary-600">
    <h3 className="text-2xl font-bold">
      {title} <RightArrow />
    </h3>
    <p className="mt-4 text-xl">{text}</p>
  </div>
)

export default Home
