import React from 'react'
import { NextPage } from 'next'
import { ComingSoon } from 'src/pages'

const Manage: NextPage = () => <ComingSoon />

export default Manage
