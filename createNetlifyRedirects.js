// eslint-disable-next-line @typescript-eslint/no-var-requires
const fs = require('fs')

const redirectFileName = '_redirects'

const redirects = [
  { from: '/api/twtr/oauth', to: '/.netlify/functions/twtr-oauth' },
  { from: '/api/*', to: '/.netlify/functions/:splat' },
]

const run = () => {
  const fileContent = redirects.map((rule) => `${rule.from} ${rule.to} ${rule.status ? rule.status : 200}`).join('\n')

  console.log('Creating Netlify redirect file with content:\n')
  console.log(fileContent)

  fs.writeFileSync(`./${redirectFileName}`, fileContent)
}

run()

/// Q: Why not have a static _redirects file in the repo?
/// A: This repo builds N Netlify sites and the redirects are only intended for the main fleeter.dev functions.
